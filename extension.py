# for setting our open and close times
from datetime import time
# for setting our start and end sessions
import pandas as pd
# for setting which days of the week we trade on
from pandas.tseries.offsets import CustomBusinessDay
# for setting our timezone
from pytz import timezone

# for creating and registering our calendar
from trading_calendars import register_calendar, TradingCalendar
from zipline.utils.memoize import lazyval

from zipline.data.bundles import register
from zipline.data.bundles.csvdir import csvdir_equities

from trading_calendars.always_open import AlwaysOpenCalendar


register(
    # name we select for the bundle
    'bitfinex',
    csvdir_equities(
        # name of the directory as specified above (named after data frequency)
        ['minute'],
        # path to directory containing the
        '/root/synq/data/bitfinex',
    ),
    calendar_name='24/7',
    start_session=pd.Timestamp('2019-01-01 00:00:00+0000', tz='utc'),
    end_session=pd.Timestamp('2019-10-31 00:00:00+0000', tz='UTC'),
    minutes_per_day=1440