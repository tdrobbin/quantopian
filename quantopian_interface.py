def make_alphalens_factor_data(factor, **kwargs):
    factor = factor.stack()
    factor.index = factor.index.set_names(['date', 'asset'])

    unv = Universe(lite=True)

    pn = unv.pn
    pricing = pn.loc[:, :, 'Close']

    import alphalens
    factor_data = alphalens.utils.get_clean_factor_and_forward_returns(
        factor, pricing, **kwargs)

    return factor_data


def get_alphalens_factor_data(view, long_short=True, periods=[1], quantiles=3):
    import alphalens

    factor = view.stack()
    factor.index = factor.index.set_names(['date', 'asset'])

    # from .data.access import read_coinmarketcap_panel, read_benchmark
    # pn = read_coinmarketcap_panel()
    # price = pn.loc[:, :, 'Close']

    # unv = Universe(lite=False)
    # price = unv.pn.loc[:, :, 'Close']

    pn = read_coinmarketcap_panel()
    price = pn.loc[view.columns, :, 'Close']


    periods = (periods,) if isinstance(periods, int) else tuple(periods)

    # print(factor.loc['2017-04-01'])
    factor_data = alphalens.utils.get_clean_factor_and_forward_returns(factor, price, quantiles=quantiles, periods=periods, filter_zscore=None)
    # print(factor_data.loc['2017-04-01'])


    return factor_data


# def get_pyfolio_input_dict(view, long_short=True, period=1, quantiles=3, capital=None):
#     # import alphalens

#     factor_data = get_alphalens_factor_data(view)

#     returns, positions, benchmark = alphalens.performance.create_pyfolio_input(factor_data=factor_data, long_short=long_short, period='{}D'.format(period), capital=capital)

#     unv = Universe(lite=False)

#     mkt_data = unv.pn.loc[:, :, ['Close', 'Volume']].copy()
#     mkt_data.rename_axis({'Close': 'price', 'Volume': 'volume'}, axis=2, inplace=True)
#     mkt_data = mkt_data.swapaxes(0, 2)

#     cap_wgt_bmk = unv.mkt_rets
#     cap_wgt_bmk = cap_wgt_bmk.reindex(returns.index)

#     return dict(returns=returns, positions=positions, benchmar  k_rets=cap_wgt_bmk, market_data=mkt_data)


def get_alphalens_pyfolio_data(view, long_short=True, period=1, quantiles=2, capital=None):
    factor_data = get_alphalens_factor_data(view, long_short=long_short, periods=period, quantiles=quantiles)

    returns, positions, benchmark = alphalens.performance.create_pyfolio_input(factor_data=factor_data, long_short=long_short, period='{}D'.format(period), capital=capital, benchmark_period=None)

    unv = Universe(lite=False)

    mkt_data = unv.pn.loc[:, :, ['Close', 'Volume']].copy()
    mkt_data.rename_axis({'Close': 'price', 'Volume': 'volume'}, axis=2, inplace=True)
    mkt_data = mkt_data.swapaxes(0, 2)

    cap_wgt_bmk = unv.mkt_rets
    cap_wgt_bmk = cap_wgt_bmk.reindex(returns.index)

    return {
        'pyfolio_dct': dict(returns=returns, positions=positions, benchmark_rets=cap_wgt_bmk, market_data=mkt_data),
        'alphalens_fac_data': factor_data
    }


def get_ic_stats(factor_data):
    ic_data = alphalens.performance.factor_information_coefficient(factor_data)

    ic_summary_table = pd.DataFrame()
    ic_summary_table["IC Mean"] = ic_data.mean()
    ic_summary_table["IC Std."] = ic_data.std()
    ic_summary_table["Risk-Adjusted IC"] = \
        ic_data.mean() / ic_data.std()
    t_stat, p_value = stats.ttest_1samp(ic_data, 0)
    ic_summary_table["t-stat(IC)"] = t_stat
    ic_summary_table["p-value(IC)"] = p_value
    ic_summary_table["IC Skew"] = stats.skew(ic_data)
    ic_summary_table["IC Kurtosis"] = stats.kurtosis(ic_data)

    return ic_summary_table
    

def run_quantopian_factor_backtest(view, rebal_freq=1, lite=True, capital=10000, title=None, description=None, tags=[],
                                   use_rebal_freq_as_periods=False):
    view_config = None

    if isinstance(view, dict):
        view_config = copy(view)
        view = make_signal(**view)

    if use_rebal_freq_as_periods:
        view = view.loc[::rebal_freq, :]

    bt_config = dict(view_config=copy(view_config), rebal_freq=rebal_freq, lite=lite, capital=capital)

    al_pf = get_alphalens_pyfolio_data(view, long_short=True, period=rebal_freq, capital=capital)
    pf_summary = pyfolio.timeseries.perf_stats(al_pf['pyfolio_dct']['returns'])

    # import pdb; pdb.set_trace()

    # add alphalens stats
    ic_summary = get_ic_stats(al_pf['alphalens_fac_data'])
    pf_summary = pf_summary.append(ic_summary.T['{}D'.format(rebal_freq)])

    autocorr = alphalens.performance.factor_rank_autocorrelation(al_pf['alphalens_fac_data'])
    pf_summary['mean factor rank autocorr'] = autocorr.mean()

    pf_summary.rename(index=lambda x: x.replace(' ', '_'), inplace=True)

    # import pdb; pdb.set_trace()
    ffn_prices = pd.DataFrame(OrderedDict((
        ('por', al_pf['pyfolio_dct']['returns']),
        ('bmk', al_pf['pyfolio_dct']['benchmark_rets']),
        ('active', al_pf['pyfolio_dct']['returns'] - al_pf['pyfolio_dct']['benchmark_rets']),
    )))

    ffn_prices = (ffn_prices + 1).cumprod(axis=0)

    rez = dict(pf_summary=pf_summary, ffn_prices=ffn_prices, returns=al_pf['pyfolio_dct']['returns'],
               benchmark_rets=al_pf['pyfolio_dct']['benchmark_rets'], positions=al_pf['pyfolio_dct']['positions'],
               factor_data=al_pf['alphalens_fac_data'], config=bt_config)

    # if lite:
    #     # return dict(pf_summary=pf_summary, ffn_prices=ffn_prices)
    #     return rez

    ffn_gs = ffn.GroupStats(ffn_prices)
    rez['ffn_gs'] = ffn_gs

    uid = get_uid()
    try:
        repo = git.Repo(REPO_BASE_DIR)
        repo_name = repo.working_dir.split('/')[-1]
        commit = repo.commit().hexsha[:8]
        branch = repo.head.reference.name

    except (git.InvalidGitRepositoryError, TypeError):
        repo_name = ''
        ommit = ''
        branch = ''

    now = str(datetime.datetime.now())
    metadata = OrderedDict(
        title=uid if title is None else title,
        description=description,
        timestamp=str(now),
        # coins=str(coins),
        start=str(ffn_prices.index[0]),
        end=str(ffn_prices.index[-1]),
        # benchmark=benchmark if isinstance(benchmark, str) else benchmark.__class__.__name__,
        # benchmark=benchmark_name,
        # no_shorting=no_shorting,
        # leverage=leverage,
        # starting_nav=starting_nav,
        # rebal_freq=str(rebal_freq),
        # tcost_model=str(tcost_model) if isinstance(tcost_model, (int, float)) else None,
        lite=lite,
        id=uid,
        tags=tags,
        git_repo_name=repo_name,
        git_commit=commit,
        git_branch=branch,
        pandas_version=pd.__version__,
        numpy_version=np.__version__,
        alphalens_version=alphalens.__version__,
        pyfolio_version=pyfolio.__version__,
        # alphalens_version=alphalens.__version__,

        # mgmt_fee=str(mgmt_fee),
        # perf_fee=str(perf_fee)
    )

    rez['metadata'] = metadata

    if lite:
        return Box(rez, box_intact_types=[ffn.core.GroupStats])

    # rez['alphalens_pyfolio_data'] = al_pf

    return Box(rez, box_intact_types=[ffn.core.GroupStats])
    # return rez
    
    
def run_diagonal_factor_backtest(fac, rets=None, factor_is_for_eod=True, rebal_freq=1, box=True):
    if isinstance(fac, dict):
        fac = make_signal(**fac)

    if rebal_freq > 1:
        fac = fac.iloc[::rebal_freq]

    if rets is None:
        close = read_coinmarketcap_panel().loc[fac.columns, fac.index, 'Close']

        # if rebal_freq > 1:
        close = close.reindex(fac.index)

        rets = close.pct_change()
    #         unv = Universe(lite=True)

    if factor_is_for_eod:
        fac = fac.shift(1)
        fac = fac.dropna(how='all', axis=0)

    # print(fac.loc['2017-04-02'])

    fac, rets = reindex_all(fac, rets)

    # print(fac.loc['2017-04-02'])

    fac_np = fac.fillna(0).as_matrix()
    rets_np = rets.fillna(0).as_matrix()

    # import pdb; pdb.set_trace()
    #     if factor_is_for_eod:
    #         eod_price_np = eod_price.pct_change().shift(-1).fillna(0).as_matrix()
    #     else:
    #         eod_price_np = eod_price.pct_change().fillna(0).as_matrix()

    ret = np.matmul(fac_np, rets_np.transpose()).diagonal()

    mean = np.mean(ret)
    vol = np.std(ret)
    sharpe = (mean / vol) * np.sqrt(365 / rebal_freq)

    bt = dict(mean=mean, vol=vol, sharpe=sharpe, rets=pd.Series(ret, index=fac.index))
    if box:
        return Box(bt)

    return bt